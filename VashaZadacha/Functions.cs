﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VashaZadacha
{
    public class Functions
    {
        public static double Pi = 3.14;
        public bool ChechForDegree(Triangle triangle)
        {
            if((triangle.c)* (triangle.c) == (triangle.b*triangle.b) + (triangle.a *triangle.a))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public double CalculateTriangle(Triangle triangle)
        {
            
            double p = (triangle.a + triangle.b + triangle.c) / 2;
            double s = (Math.Sqrt(p * (p-triangle.a) * (p-triangle.b) * (p-triangle.c)));
            return s;
        }
        public double PloshadKruga(Circle circle)
        {
            double s = Pi * circle.Radius * circle.Radius;
            return s;
        }
    }
}
